<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

/** esempio */
$router->get('/saluta', function () use ($router) {
    return 'Ciao ragazzi!!';
});

/** andiamo a fare cose più serie!!!
 * Questa ritorna l'elenco dei todos in formato JSON [GET]
 */
$router->get('/api/todos', 'TodosController@list');

/**
 * Questa crea un nuovo todo e lo inserisce nella tabella todos [POST]
 */
$router->post('/api/todos', 'TodosController@create');

/**
 * Questa aggiorna un tood esistente passando l'{id} del todo come parametro nell'URL [PUT]
 */
$router->put('/api/todos/{id}', 'TodosController@update');

/**
 * Questa elimina un tood esistente passando l'{id} del todo come parametro nell'URL [DELETE]
 */
$router->delete('/api/todos/{id}', 'TodosController@delete');