<?php

namespace App\Http\Controllers;

/** serve per accedere all'oggeto request che contiene i dati forniti dal client */
use Illuminate\Http\Request;
/** serve per personalizzare il tipo di risposta HTTP */
use Illuminate\Http\Response;

class TodosController extends Controller
{
    /**
     * Questo controller gestisce la tabella todos
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    /** questo metodo ritorna l'elenco dei todos in JSON 
     *  questa viene chiamata con /api/todos [GET]
    */
    public function list()
    {
        $results = app('db')->select("SELECT * FROM todos");
        return $results;
    }

     /** questo metodo crea un record todo nella tabella todos 
      * questa viene chiamata con /apis/todos [POST]
     */
     public function create(Request $request)
     {
         // recupero il valore di description che l'utente passa in [POST]
         // e lo metto nella variabile $description
        $description = $request->input('description');
        $expireDate = $request->input('expire_date');

        if($expireDate){
            // se viene passato expireDate eseguo la query considerando questo campo
            $results = app('db')->insert("
                INSERT INTO `todos` (`description`, `done`, `insert_date`, `expire_date`) 
                VALUES ('$description', '0', now(), '$expireDate');
            ");
        }
        else{
            // altrimenti inserisco il todo senza expireDate
            $results = app('db')->insert("
                INSERT INTO `todos` (`description`, `done`, `insert_date`) 
                VALUES ('$description', '0', now());
            ");
        }
        
        // ritorno una risposta http
        return new Response(null, 200);
     }

     /** questo metodo aggiorna un record todo nella tabella todos 
      * questa viene chiamata con /api/todos/{id} [PUT]
     *                                       |
     *                                       V
     */
    public function update(Request $request,$id)
    {
        // recupero il valore di description che l'utente passa in [PUT]
        // e lo metto nella variabile $description
        $description = $request->input('description');
        $done =  $request->input('done');
        $expireDate = $request->input('expire_date');
        
        if($expireDate){ //da falso anche se è null
            // se expireDate esiste
            $results = app('db')->update("
                UPDATE `todos`
                SET 
                    `description` = '$description',
                    `done` = $done,
                    `expire_date` = '$expireDate'
                WHERE 
                    `todos`.`id` = $id;
            ");
        }
        else{
            // se expireDate non esiste aggiorno senza il campo expireDate
            $results = app('db')->update("
                UPDATE `todos`
                SET 
                    `description` = '$description',
                    `done` = $done,
                    `expire_date` = null
                WHERE 
                    `todos`.`id` = $id;
            ");
        }
       // ritorno una risposta http
       return new Response(null, 200);
    }

     /** questo metodo elimina un record todo dalla tabella todos 
      * questa viene chiamata con /api/todos/{id} [DELETE]
     *                                       |
     *                                       V
     */
    public function delete(Request $request,$id)
    {
        $results = app('db')->delete("
            DELETE FROM todos WHERE id = $id
        ");
       // ritorno una risposta http
       return new Response(null, 204);
    }
}
