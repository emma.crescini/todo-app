console.log("file js caricato");

//dichiaro la funzione async
//che permette di eseguire in modo "sincrono"
// le istruzioni dopo l'await

async function addTodo(){
    //qui ci sarà il metodo per aggiungiere un todo [post]

    //PRIMO MODO
    //var fetchConfig=new Object();
    //fetchConfig.method="POST";

    //Altro modo
    //var fetchConfig={
        //method: "POST"
    //}
    //await fetch("/api/todos",fetchConfig);

    var inputEl=document.getElementById("addTodoInput");
    var description=inputEl.value; //recupero il valore inserito nella casella di testo
    var expiredateEl=document.getElementById("expiredateInput");
    var expireDate=expiredateEl.value;

    //ALTRO METODO
    await fetch("/api/todos",{ 
        method: "POST",
        //aggiungo un header contente il content type 
        //perché così il server capisce
        headers:{ //indica che passiamo un body json
            "Content-Type":"application/json"
        },
        //passo un oggetto con la proprietà description che contiene il todo
        //NB l'oggetto da passare deve essere serializzato
        body: JSON.stringify({
            description:description,
            expire_date: expireDate
        })
    });

    // Pulisco la casella di testo
    inputEl.value = "";
    expiredateEl.value = "";

    //ricarico tutti i todo
    renderTodos();
}

//questa funzione elimina un todo chiamando HTTP [DELETE] passando l'id del todo da eliminare
//api/todos/{id}
async function deleteTodo(id){
    var response=await fetch("/api/todos/"+id,{
        method: "DELETE"
    });

    renderTodos();

}

// Questa funzione salva un todo chiamando HTTP [PUT] passando l'id del todo da eliminare
// API/TODOS/{id}
async function saveTodo(id, inputDescription,expireDateEl,checkboxEl) {
    var description = inputDescription.value;
    var expireDate=expireDateEl.innerText;
    var done=checkboxEl.checked;
    var response=await fetch("/api/todos/"+id,{
        method: "PUT",
        headers:{
            "Content-Type":"application/json" 
        },
        body: JSON.stringify({
            description:description,
            done: done ? 1 : 0,
            expire_date: expireDate
        })
    });


    // Ricarico tutto
    renderTodos();
}

//ricarica i todo
async function renderTodos(){
    var ulElement=document.getElementById("todosListUl");
    var response=await fetch("/api/todos");
    var todos=await response.json();

    //svuoto la lista dei todo
    ulElement.innerHTML="";

    //for per stampare i todos nella pagina html
    for(var i=0; i<todos.length; i++){
        var todo=todos[i]; //prendo il todo-iesimo
        var liElement=document.createElement('li');
        var deleteButton=document.createElement("i");
        var saveButton=document.createElement("i");
        var divDescription=document.createElement("div");
        var inputDescription=document.createElement("input");
        inputDescription.type="text";
        var checkboxEl=document.createElement("input"); //todo completo si/no
        checkboxEl.type="checkbox"; //lo fa diventare un checkbox
        //liElement.style="margin: 10px 0px";
        //deleteButton.innerText="X";
        //deleteButton.style="margin-right: 10px";
        deleteButton.className="mdi mdi-delete";
        inputDescription.value=todo.description;
        //inputDescription.style="border: none";
        //saveButton.innerText="SALVA";
        saveButton.className="mdi mdi-content-save";
        saveButton.style= "margin-right: 10px";
        divDescription.appendChild(deleteButton);
        divDescription.appendChild(saveButton);
        divDescription.appendChild(checkboxEl); //lo appendo dopo il floppy
        divDescription.appendChild(inputDescription);
        liElement.appendChild(divDescription);

        //quando l'utente clicca il tasto x chiama la funzione deleteTodo
        var deleteThisTodo=deleteTodo.bind(null,todo.id); //funzione con parametri prestabiliti
        deleteButton.addEventListener("click",deleteThisTodo);
        

        var divExpireDate = document.createElement('div');

        if(todo.expire_date!=null){
            divExpireDate.innerText = todo.expire_date;
            divExpireDate.className="expireDate";
            //divExpireDate.style = "font-size: 11px; color:red;";
            liElement.appendChild(divExpireDate);
            //liElement.innerHTML += "<div style = 'font-size: 11px;'>" + todo.EXPIRE_DATE + "</div>";
        }

        //check l'input type check quando done=1
        if(todo.done===1){
            checkboxEl.checked=true;
        }

        var saveThisTodo = saveTodo.bind(null, todo.id, inputDescription,divExpireDate,checkboxEl);
        saveButton.addEventListener('click', saveThisTodo);
        ulElement.appendChild(liElement);
    }
}

async function main(){
    console.log("documento caricato completamente");
    //var variabile;
    //let variabile2;
    //const variabile3; //costante

    //prendo il tag div con id=app
    var appDiv=document.getElementById("app");
    
    //creo il tag input che mi permette di scrivere il nuovo todo
    var inputEl=document.createElement("input");
    inputEl.type="text"; //si imposta il tipo che è una casella di testo
    inputEl.placeholder="Inserisci il testo del todo"; //inserisce un'indicazione
    inputEl.id="addTodoInput"; //aggiungo l'id perché mi servirà prenderlo successivamente
    appDiv.appendChild(inputEl); //aggiungo la casella di testo al div

    //creo la casella di testo per mettere la scadenza
    var expireDateEl=document.createElement('input');
    expireDateEl.id="expiredateInput";
    expireDateEl.type="datetime-local";
    appDiv.appendChild(expireDateEl);

    var buttonEl=document.createElement("button");
    buttonEl.innerText="Inserisci"; //si può usare anche textContent
    appDiv.appendChild(buttonEl);

    //aggiungo l'evento click al bottone
    buttonEl.addEventListener("click",addTodo);

    //creo il tag ul
    var ulElement=document.createElement('ul');
    ulElement.id="todosListUl";
    //aggiungo dentro il tag div app l'elemento ul
    appDiv.appendChild(ulElement);
    
    //una volta costruita la mia interfaccia
    //carico e renderizzo i todos
    renderTodos(); //richiamo la funzione per caricare i todos
}

//aspetto che la pagina sia stata caricata completamente
//si può usare il DOMContentLoaded o readystatechange
document.addEventListener("DOMContentLoaded",main);